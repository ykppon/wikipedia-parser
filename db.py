from sqlalchemy import create_engine, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine('sqlite:///./base.sqlite', echo=True)
Base = declarative_base()

class Page(Base):
  __tablename__ = 'pages'

  id = Column(Integer, primary_key=True)
  url = Column(String)
  request_depth = Column(Integer)

class Connection(Base):
  __tablename__ = 'connections'

  from_page_id = Column(Integer, ForeignKey('pages.id'), primary_key=True)
  link_id = Column(Integer, ForeignKey('pages.id'), primary_key=True)

Base.metadata.create_all(engine)


async def writeLinks(parent, url, depth):
  Session = sessionmaker()
  Session.configure(bind=engine)
  db = Session()
  link = Page(url=url, request_depth=depth)
  db.add(link)
  parId = db.query(Page).filter_by(url=parent).first()
  if depth == 0:
    parId.id = 1
  connection = Connection(from_page_id=parId.id, link_id=link.id)
  db.add(connection)
  db.commit()
  db.close()
  return(url)