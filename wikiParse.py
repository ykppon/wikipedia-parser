import asyncio, aiohttp, re
from db import writeLinks

url = '/wiki/Catuvellauni' # url использовал без домена

async def fetchPage(url, session):
  async with session.get(url) as response:
    return await response.read()

async def findLinks(page):
  # return re.findall(r'<a href="#(\S*)"', str(await asyncio.gather(task))) - при таком поиске мы получаем не списки статей, а только ссылки навигации по этой статье это же якоря поэтому я буду искать по изменненной регулярке
  return re.findall(r'<a href="(/wiki/(?!File:|Wikipedia:|Special:|Portal:|Help:|Category:|Talk:|Template:|Main_Page)\S*)"', str(page)) # при таком запросе мы получаем именно статьи на вики

tasks = []

async def wikiParse(url, rec=0):
  exturl = 'https://en.wikipedia.org'+url # использовал английскую версию, чтобы в таблицу писались данные с латиницей, чем то что получаетяс при кириллическом url
  async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:  # SSL сертификат кричал что не может пройти валидцию, поэтому отключил её
    if rec == 2: # тут определяется глубина на которой заканчивается парсинг
      return(200)
    task = asyncio.gather(fetchPage(exturl.format(), session))
    tasks.append(task)
    page = await asyncio.gather(task) # Получаю содержимое страницы
    if rec == 0:  # Записываю первую ссылку
      await asyncio.gather(writeLinks(url, url, rec))
    list = await asyncio.gather(findLinks(page))
    rec += 1
    for item in list[0]: # проход по ссылкам с занесением их в таблицу
      new = await asyncio.gather(writeLinks(url, item, rec))
      tasks.append(new)
      recourse = asyncio.ensure_future(wikiParse(str(item), rec)) # запускаю рекурсивно для дочерних элементов
      tasks.append(recourse)

loop = asyncio.get_event_loop()
future = asyncio.ensure_future(wikiParse(url))
loop.run_until_complete(future)
